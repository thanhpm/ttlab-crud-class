'use strict';

const faker = require('faker');

module.exports = {
  up: (queryInterface, Sequelize) => {
      let data = [];
      let amount = 100;

      while(amount--){

        let date = new Date();
        data.push({
          schoolId: faker.random.number({'min':1,'max':100}),
          name: faker.random.number({'min':1,'max':12}) + 'A' + faker.random.number({'min':1,'max':10}),
          description: "Hahhahahahahahhahahahahahha",
          createdAt: date,
          updatedAt: date
        });
      }

      return queryInterface.bulkInsert('Classes', data, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Classes', null, {})
  }
};