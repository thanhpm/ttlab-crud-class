module.exports = (sequelize, DataTypes) => {
  const Classes = sequelize.define('Classes', {
      schoolId: {
          type: DataTypes.INTEGER,
          allowNull: false,
      },
      name: {
          type: DataTypes.STRING,
          allowNull: true,
      },
      description: {
          type: DataTypes.STRING,
          allowNull: true,
      }
  }, {
      tableName: 'Classes',
  });
  Classes.associate = function (models) {
    Classes.belongsTo(models.Schools, {
      foreignKey: 'schoolId',
  });
  };
  return Classes;
};
