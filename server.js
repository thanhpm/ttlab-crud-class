import sequelize from 'sequelize';
import Model from './models';
import express from 'express';

const app = express();

let transaction;    

app.get('/getDetailSchool', async (req, res) =>{
    transaction = await sequelize.transaction();

    try{
        let data = await Model.Schools.findAll({ 
            include: Model.Classes,
            limit:1
         },transaction)
    }
    catch(err){
        if(transaction) await transaction.rollback();
    }
})

app.get('/getDetailClass', async (req, res) =>{
    try{
        let data = await Model.Schools.findAll({ 
            include: Model.Schools,
            limit:1
         },transaction)
         res.json(data);
    }
    catch(err){
        if(transaction) await transaction.rollback();
        res.json("ERROR");
    }

})

app.delete('/:id', async (req, res) =>{
    const transaction = await Model.sequelize.transaction({autocommit:false});
    const id = req.params.id;

    try{
        let data = await Model.Schools.destroy({ 
            where:{
                id
            }
         },transaction)
        await transaction.commit();
        res.json("DONE");
    }
    catch(err){
        if(transaction) await transaction.rollback();
        res.json("ERROR")
    }
})




app.listen(3000, (req, res) =>{
    console.log('RUNNING...')
})